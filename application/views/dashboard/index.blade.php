@extends('layouts.global')

@section('title', "Dashboard")

@section('pageTitle', 'Dashboard')
@section('pageTitleIcon', 'fa-home')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <hr class="my-3">
            <div class="row">
                <div class="col-md-12">
                    @component('components.flash.notify')
                    @endcomponent
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-md-12 text-right">

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <form method="GET" action="">
                        <select name="filter-lapangan" id="filter-lapangan" class="form-control">
                            <option value="" disabled selected>Filter Berdasarkan Lapangan</option>
                            @foreach($lapangan as $lp)
                                <option value="{{ $lp->id_lapangan }}">{{ $lp->nama_lapangan }}</option>
                            @endforeach
                        </select>
                    </form>
                </div>
                <div class="col-md-6">
                    {{-- <ul class="nav nav-pills card-header-pills">
                        <li class="nav-item">
                            <a class="nav-link" href="#">All</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Publish</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Draft</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Trash</a>
                        </li>
                    </ul> --}}
                </div>
            </div>
            <hr class="my-3">
            <div class="row mb-3">
                <div class="col-md-12 text-right">
                    @component('components.buttons.add', ['link' => 'sewa/add', 'text' => 'Buat Pesanan'])

                    @endcomponent
                </div>
            </div>
            <table class="table table-bordered table-stripped">
                <thead>
                    <tr class="text-center">
                        <th class="text-middle" rowspan="2"><b>Jam</b></th>
                        <th colspan="4"><strong>Lapangan</strong></th>
                    </tr>
                    <tr class="text-center">
                    	@foreach($lapangan as $lp)
	                        <td class="text-middle text-capitalize"><strong>{{ $lp->nama_lapangan }}</strong></td>
	                    @endforeach
                    </tr>
                </thead>
                <tbody class="text-center">
                	@foreach($jadwal as $jam)
	                    <tr>
	                        <td class="text-middle">{{ $jam->jadwal_jam_lapangan }}</td>
	                        <td><span class="badge bg-danger text-white">booked</span></td>
	                        <td><span class="badge bg-danger text-white">booked</span></td>
	                        <td><a href="#" class="btn btn-xs btn-info">Pesan</a></td>
	                    </tr>
	                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">

                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
