<?php
defined('BASEPATH') or exit('No direct script access allowed');

class JadwalController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->auth->isLogin();
		$this->middleware->protect(['create', 'edit', 'store', 'update', 'destroy']);
		$this->load->model('JadwalModel', 'Model');
		$this->session->set_userdata('active', 'jadwal');
	}

	/**
	 * Get all data from db
	 * @return  mixed
	 */
	public function index()
	{
		$data = $this->Model->get(false, ['jadwal_jam_lapangan', 'ASC']);

		return view('jadwal.index', compact('data'));
	}

	/**
	 * rendering view for form to create data
	 * @return  mixed
	 */
	public function create()
	{
		return view('jadwal.create');
	}

	/**
	 * storing data to database from request
	 * @return  mixed
	 */
	public function store()
	{

		// storing data from request to database
		$this->load->library('FormValidation');

		if(!($this->formvalidation->validate('jadwal'))) {
			setFlashMessage('Data gagal ditambahkan', 'danger');
			redirect(base_url("jadwal/add"));
		}

		$result = $this->Model->store($this->input->post(null, true));

		if(!$result) {
			setFlashMessage('Data gagal ditambahkan', 'danger');
			redirect(base_url("jadwal/add"));
		}

		setFlashMessage('Data bershasil ditambahkan', 'success');
		redirect(base_url("jadwal"));
	}

	/**
	 * rendering view for form editing data
	 * @param  string $id
	 * @return  mixed
	 */
	public function edit(string $id)
	{
		// render view for edit existing data
		$data = $this->Model->find($id);

		return view('jadwal.edit', compact('data'));
	}

	/**
	 * storing updated data to database from request
	 * @param  string $id
	 * @return  mixed
	 */
	public function update(string $id)
	{
		$data = $this->input->post(null, true);

		// update data from request to database
		$this->load->library('FormValidation');
		$jadwal = $this->formvalidation->load('jadwal');

		if(!($this->formvalidation->validate('jadwal')) || !($jadwal->checkTimeFormat($data))) {
			setFlashMessage('Data gagal diupdate', 'danger');
			redirect(base_url("jadwal/{$id}/update"));
		}

		$result = $this->Model->update($data, $id);

		if(!$result) {
			setFlashMessage('Data gagal diupdate', 'danger');
			redirect(base_url("jadwal/{$id}/update"));
		}

		setFlashMessage('Data bershasil diupdate', 'success');
		redirect(base_url("jadwal"));
	}

	/**
	 * deleting data from database
	 * @param  string $id
	 * @return  mixed
	 */
	public function destroy(string $id)
	{
		// destroy data from database
		$result = $this->Model->destroy($id);

		if(!$result) {
			setFlashMessage('Data gagal di hapus', 'danger');
			redirect(base_url("jadwal/{$id}/show"));
		}

		setFlashMessage('Data bershasil di hapus', 'success');
		redirect(base_url("jadwal"));
	}
}
