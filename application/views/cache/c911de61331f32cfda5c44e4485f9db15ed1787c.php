<?php $__env->startSection("title", "Tambah Jenis Lapangan"); ?>

<?php $__env->startSection("content"); ?>
    <div class="row">
        <div class="col-md-8">
            <?php $__env->startComponent('components.flash.notify'); ?>
            <?php echo $__env->renderComponent(); ?>

            <form action="<?php echo e(base_url('jenis-lapangan')); ?>" method="POST" class="shadow-sm p-3 bg-white">
                <div class="form-group">
                    <?php $__env->startComponent('components.form.input', [
                        'type' => 'text',
                        'id' => 'nama-jlp',
                        'placeholder' => 'Nama Jenis Lapangan',
                        'name' => 'nama_jenis_lapangan',
                        'label' => 'Nama Jenis Lapangan'
                    ]); ?>
                        
                    <?php echo $__env->renderComponent(); ?>
                </div>

                <div class="form-group">
                    <?php $__env->startComponent('components.form.input', [
                        'type' => 'number',
                        'id' => 'harga-jlp',
                        'placeholder' => 'Harga',
                        'name' => 'harga',
                        'label' => 'Harga'
                    ]); ?>
                        
                    <?php echo $__env->renderComponent(); ?>
                </div>

                <div class="form-group">
                    <?php echo getCSRFToken(); ?>

                    <?php $__env->startComponent('components.buttons.submit', ['text' => 'Tambah', 'type' => 'primary']); ?><?php echo $__env->renderComponent(); ?>
                    <?php $__env->startComponent('components.buttons.cancel', ['text' => 'Batal', 'type' => 'secondary', 'link' => 'jenis-lapangan']); ?><?php echo $__env->renderComponent(); ?>
                </div>
            </form>
        </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("layouts.global", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /opt/lampp/htdocs/futsal-falad/application/views/jenis-lapangan/create.blade.php */ ?>