<label for="<?php echo e($id); ?>"><?php echo e($label); ?></label>
<div class="input-group date" id="datetimepicker3" data-target-input="nearest">
	<div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
    <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
	</div>

	<input type="text" class="form-control datetimepicker-input
	<?php echo e(error($name) ? "is-invalid" : ""); ?>" data-target="#datetimepicker3"
	name="<?php echo e($name); ?>" id="<?php echo e($id); ?>" value="<?php echo e(isset($data->{$name}) ? $data->{$name} : oldValue($name) ?: ''); ?>" />

	<?php echo error($name); ?>

</div>

<?php $__env->startSection('style'); ?>
	<link rel="stylesheet" href="<?php echo e(base_url('assets/css/tempusdominus-bootstrap-4.min.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
	<script src="<?php echo e(base_url('assets/js/moment.js')); ?>"></script>
	<script src="<?php echo e(base_url('assets/js/tempusdominus-bootstrap-4.min.js')); ?>"></script>
	<script src="<?php echo e(base_url('assets/js/custom.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php /* /opt/lampp/htdocs/futsal-falad/application/views/components/form/time-picker.blade.php */ ?>