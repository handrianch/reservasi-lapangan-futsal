<?php
namespace App\Validations;

abstract class Validation
{
  protected $instance = null;

  public function run($instance)
  {
    $this->instance = $instance;

    $this->except();
    $this->setRules();
    $this->setDelimiters();

    if(!($this->instance->form_validation->run())) {
      $errors = $this->getErrors();
      $oldValue = $this->getOldValue();

      $this->instance->session->set_flashdata('errors', $errors);
      $this->instance->session->set_flashdata('oldValue', $oldValue);

      return false;
    }

    return true;
  }

  private function setRules()
  {
    foreach($this->config as $conf) {
      $this->instance->form_validation->set_rules(...$conf);
    }
  }

  private function setDelimiters()
  {
    $this->instance->form_validation->set_error_delimiters('<div class="invalid-feedback">', '</div>');
  }

  abstract public function except();
  abstract public function getErrors();
  abstract public function getOldValue();
}
