<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9 no-js" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Palad Futsal | @yield("title")</title>
		<link rel="stylesheet" href="{{ base_url('assets/css/polished.min.css') }}">
		<link rel="stylesheet" href="{{ base_url('assets/css/custom.css') }}">
		<link rel="stylesheet" href="{{ base_url('assets/css/responsive.css') }}">
		<link rel="stylesheet" href="{{ base_url('assets/vendors/font-awesome/css/font-awesome.min.css') }}">
		@yield('style')
		<style>
			.grid-highlight {
				padding-top: 1rem;
				padding-bottom: 1rem;
				background-color: #5c6ac4;
				border: 1px solid #202e78;
				color: #fff;
			}

			hr {
				margin: 6rem 0;
			}

			hr+.display-3,
			hr+.display-2+.display-3 {
				margin-bottom: 2rem;
			}
		</style>
		<script type="text/javascript">
			document.documentElement.className = document.documentElement.className.replace('no-js', 'js') + (document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1") ? ' svg' : ' no-svg');
		</script>
	</head>
	<body>
		<nav class="navbar navbar-expand p-0">
			<a class="navbar-brand text-center col-xs-12 col-md-3 col-lg-2 mr-0" href="{{ base_url() }}"> Palad <span class="fa fa-futbol-o"></span> Futsal </a>
			<button class="btn btn-link d-block d-md-none" data-toggle="collapse" data-target="#sidebar-nav" role="button" >
				<span class="oi oi-menu"></span>
			</button>
			<div class="col-md-10">
				<form action="{{ base_url('logout') }}" method="POST" class="shadow-bg d-inline-block float-right">
					{!! getCSRFToken() !!}
					<button class="btn btn-danger btn-sm" style="cursor:pointer" onclick="confirm('Anda yakin?')"> <span class="fa fa-sign-out"></span> SignOut</button>
				</form>
			</div>
		</nav>

		<div class="container-fluid h-100 p-0">
			<div style="min-height: 100%" class="flex-row d-flex align-itemsstretch m-0">
				@include('layouts.components.sidebar')

				<div class="col-lg-10 col-md-9 p-4">
					<div class="row ">
						<div class="col-md-12 pl-3 pt-2">
							<div class="pl-3">
								<h3 class="display-4"><span class="fa @yield('pageTitleIcon')"></span> <small>@yield("pageTitle")</small></h3>
							</div>
						</div>
					</div>
					@yield("content")
				</div>
			</div>
		</div>

		<script src="{{ base_url("assets/js/jquery-3.3.1.min.js") }}"></script>
		<script src="{{ base_url("assets/js/popper.min.js") }}"></script>
		<script src="{{ base_url("assets/js/bootstrap.min.js") }}"></script>
		@yield("footer-script")
		@yield('script')
	</body>
</html>
