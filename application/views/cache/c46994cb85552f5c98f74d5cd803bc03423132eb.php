<?php $__env->startSection('title', "Dashboard"); ?>

<?php $__env->startSection('pageTitle', 'Dashboard'); ?>
<?php $__env->startSection('pageTitleIcon', 'fa-home'); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <hr class="my-3">
            <div class="row">
                <div class="col-md-12">
                    <?php $__env->startComponent('components.flash.notify'); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-md-12 text-right">

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <form method="GET" action="">
                        <select name="filter-lapangan" id="filter-lapangan" class="form-control">
                            <option value="" disabled selected>Filter Berdasarkan Lapangan</option>
                            <?php $__currentLoopData = $lapangan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($lp->id_lapangan); ?>"><?php echo e($lp->nama_lapangan); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </form>
                </div>
                <div class="col-md-6">
                    
                </div>
            </div>
            <hr class="my-3">
            <div class="row mb-3">
                <div class="col-md-12 text-right">
                    <?php $__env->startComponent('components.buttons.add', ['link' => 'sewa/add', 'text' => 'Buat Pesanan']); ?>

                    <?php echo $__env->renderComponent(); ?>
                </div>
            </div>
            <table class="table table-bordered table-stripped">
                <thead>
                    <tr class="text-center">
                        <th class="text-middle" rowspan="2"><b>Jam</b></th>
                        <th colspan="4"><strong>Lapangan</strong></th>
                    </tr>
                    <tr class="text-center">
                    	<?php $__currentLoopData = $lapangan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	                        <td class="text-middle text-capitalize"><strong><?php echo e($lp->nama_lapangan); ?></strong></td>
	                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tr>
                </thead>
                <tbody class="text-center">
                	<?php $__currentLoopData = $jadwal; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jam): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	                    <tr>
	                        <td class="text-middle"><?php echo e($jam->jadwal_jam_lapangan); ?></td>
	                        <td><span class="badge bg-danger text-white">booked</span></td>
	                        <td><span class="badge bg-danger text-white">booked</span></td>
	                        <td><a href="#" class="btn btn-xs btn-info">Pesan</a></td>
	                    </tr>
	                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">

                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.global', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /opt/lampp/htdocs/futsal-falad/application/views/dashboard/index.blade.php */ ?>