<label for="<?php echo e($id); ?>"><?php echo e($label); ?></label>
<input type="<?php echo e($type); ?>" class="form-control <?php echo e(error($name) ? "is-invalid" : ""); ?>" name="<?php echo e($name); ?>" id="<?php echo e($id); ?>" 
value="<?php echo e(isset($data->{$name}) ? $data->{$name} : oldValue($name) ?: ''); ?>" placeholder="<?php echo e($placeholder); ?>">

<?php echo error($name); ?>
<?php /* /opt/lampp/htdocs/futsal-falad/application/views/components/form/input.blade.php */ ?>