<?php $__env->startSection('title', "Dashboard"); ?>

<?php $__env->startSection('pageTitle', 'Dashboard'); ?>
<?php $__env->startSection('pageTitleIcon', 'fa-home'); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <hr class="my-3">
            <div class="row">
                <div class="col-md-12">
                    <?php $__env->startComponent('components.flash.notify'); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-md-12 text-right">
                    
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.global', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /opt/lampp/htdocs/futsal-falad/application/views/dashboard.blade.php */ ?>