<?php
use Models\Model;

class JadwalModel extends Model
{
  protected $table = 'jadwal_lapangan';
	protected $primaryKey = 'id_jadwal_lapangan';
	protected $fillable = ['jadwal_jam_lapangan'];

	public function get(bool $type = false, array $orderBy = [])
	{
		if(count($orderBy) <= 0) {
			$orderBy = [$this->primaryKey, 'ASC'];
		}

		$query = $this->db->select("$this->primaryKey, DATE_FORMAT(jadwal_jam_lapangan, '%H:%i') as jadwal_jam_lapangan")
										  ->from($this->table)
										  ->order_by(...$orderBy)
										  ->get();

		if($type) {
			return $query->result_array();
		}

		return $query->result();
	}

	public function find(string $value = '', string $field = '')
	{
		$field = $field ?: $this->primaryKey;
		return $this->db->select("$this->primaryKey, DATE_FORMAT(jadwal_jam_lapangan, '%H:%i') as jadwal_jam_lapangan")
										->from($this->table)
										->where($field, $value)
										->get()
										->row();
	}


	protected function setUp()
	{
		return [
			'id_jadwal_lapangan' 	=> $this->generateID()
		];
	}

	protected function generateID()
	{
		$jam = $this->input->post('jadwal_jam_lapangan', true);

		if($jam < 12) {
			$tipe = 'P';
		} elseif($jam < 18) {
			$tipe = 'S';
		} elseif($jam <= 23) {
			$tipe = 'M';
		}

		$oldID = $this->db->select("right($this->primaryKey, 2) as kode")
											->order_by("right($this->primaryKey, 2) DESC")
											->limit(1)
											->get($this->table)
											->row();

		if($oldID) {
			$newID = intval($oldID->kode) + 1;
			$finalID = $tipe . str_pad($newID, 2, "0", STR_PAD_LEFT);
			return $finalID;
		}

		return "{$tipe}01";
	}
}
