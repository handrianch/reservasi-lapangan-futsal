<?php $__env->startSection("title", "Tambah Admin"); ?>

<?php $__env->startSection("content"); ?>
    <div class="row">
        <div class="col-md-8">
            <?php $__env->startComponent('components.flash.notify'); ?>
            <?php echo $__env->renderComponent(); ?>

            <form action="<?php echo e(base_url('admin')); ?>" method="POST" class="shadow-sm p-3 bg-white">
                <div class="form-group">
                    <?php $__env->startComponent('components.form.input', [
                        'type' => 'text',
                        'id' => 'nama-adm',
                        'placeholder' => 'Nama',
                        'name' => 'nama_admin',
                        'label' => 'Nama Admin'
                    ]); ?>
                        
                    <?php echo $__env->renderComponent(); ?>
                </div>

                <div class="form-group">
                    <?php $__env->startComponent('components.form.input', [
                        'type' => 'text',
                        'id' => 'username-adm',
                        'placeholder' => 'Username',
                        'name' => 'username',
                        'label' => 'Username'
                    ]); ?>
                        
                    <?php echo $__env->renderComponent(); ?>
                </div>

                <div class="form-group">
                    <?php $__env->startComponent('components.form.input', [
                        'type' => 'password',
                        'id' => 'password-adm',
                        'placeholder' => 'Password Admin',
                        'name' => 'password',
                        'label' => 'Password'
                    ]); ?>
                        
                    <?php echo $__env->renderComponent(); ?>
                </div>

                <div class="form-group">
                    <?php $__env->startComponent('components.form.input', [
                        'type' => 'password',
                        'id' => 're-password-adm',
                        'placeholder' => 'Re-Password Admin',
                        'name' => 're-password',
                        'label' => 'Re-Password'
                    ]); ?>
                        
                    <?php echo $__env->renderComponent(); ?>
                </div>

                <div class="form-group">
                    <label>Tipe Akun : </label> <br />
                    
                    <?php $__env->startComponent('components.form.radio', [
                        'label' => 'Admin',
                        'id' => 'adm',
                        'name' => 'tipe-akun',
                        'value' => 'adm',
                        'checked' => 'checked'
                    ]); ?><?php echo $__env->renderComponent(); ?>

                    <?php $__env->startComponent('components.form.radio', [
                        'label' => 'Super Admin',
                        'id' => 'su',
                        'name' => 'tipe-akun',
                        'value' => 'su',
                        'checked' => ''
                    ]); ?><?php echo $__env->renderComponent(); ?>
                </div>

                <div class="form-group">
                    <?php echo getCSRFToken(); ?>

                    <?php $__env->startComponent('components.buttons.submit', ['text' => 'Tambah', 'type' => 'primary']); ?><?php echo $__env->renderComponent(); ?>
                    <?php $__env->startComponent('components.buttons.cancel', ['text' => 'Batal', 'type' => 'secondary', 'link' => 'admin']); ?><?php echo $__env->renderComponent(); ?>
                </div>
            </form>
        </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("layouts.global", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /opt/lampp/htdocs/futsal-falad/application/views/admin/create.blade.php */ ?>