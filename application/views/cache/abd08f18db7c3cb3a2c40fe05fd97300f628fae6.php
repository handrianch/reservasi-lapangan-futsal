<?php $__env->startSection("title", "Tambah Admin"); ?>

<?php $__env->startSection("content"); ?>
    <div class="row">
        <div class="col-md-8">
            <?php $__env->startComponent('components.flash.notify'); ?>
            <?php echo $__env->renderComponent(); ?>

            <form action="<?php echo e(base_url('admin')); ?>" method="POST" class="shadow-sm p-3 bg-white">
                <div class="form-group">
                    <?php $__env->startComponent('components.form.input', [
                        'type' => 'text',
                        'id' => 'nama-penyewa',
                        'placeholder' => 'Nama Penyewa',
                        'name' => 'nama_penyewa',
                        'label' => 'Nama Penyewa'
                    ]); ?>
                        
                    <?php echo $__env->renderComponent(); ?>
                </div>

                <div class="form-group">
                    <?php $__env->startComponent('components.form.textarea', [
                        'id' => 'alamat-penyewa',
                        'placeholder' => 'Alamat Penyewa',
                        'name' => 'alamat_penyewa',
                        'label' => 'Alamat Penyewa'
                    ]); ?>
                        
                    <?php echo $__env->renderComponent(); ?>
                </div>

                <div class="form-group">
                    <label for="">Pilih Lapangan</label>
                    <select name="" id="" class="form-control">
                        <option value="">Lapangan 1</option>
                    </select>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php $__env->startComponent('components.form.input', [
                                'type' => 'hours',
                                'id' => 'lama-sewa',
                                'placeholder' => 'Lama Sewa',
                                'name' => 'lama-sewa',
                                'label' => 'Lama Sewa'
                            ]); ?>
                        
                            <?php echo $__env->renderComponent(); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Mulai Jam : </label>
                            <select name="" id="" class="form-control">
                                <option value="">08 : 00</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Status Bayar : </label> <br />
                    
                    <?php $__env->startComponent('components.form.radio', [
                        'label' => 'Cash',
                        'id' => 'adm',
                        'name' => 'tipe-akun',
                        'value' => 'adm',
                        'checked' => 'checked'
                    ]); ?><?php echo $__env->renderComponent(); ?>

                    <?php $__env->startComponent('components.form.radio', [
                        'label' => 'On Proses',
                        'id' => 'su',
                        'name' => 'tipe-akun',
                        'value' => 'su',
                        'checked' => ''
                    ]); ?><?php echo $__env->renderComponent(); ?>

                    <?php $__env->startComponent('components.form.radio', [
                        'label' => 'DP',
                        'id' => 'su',
                        'name' => 'tipe-akun',
                        'value' => 'su',
                        'checked' => ''
                    ]); ?><?php echo $__env->renderComponent(); ?>
                </div>

                <div class="form-group">
                    <?php echo getCSRFToken(); ?>

                    <?php $__env->startComponent('components.buttons.submit', ['text' => 'Tambah', 'type' => 'primary']); ?><?php echo $__env->renderComponent(); ?>
                    <?php $__env->startComponent('components.buttons.cancel', ['text' => 'Batal', 'type' => 'secondary', 'link' => 'admin']); ?><?php echo $__env->renderComponent(); ?>
                </div>
            </form>
        </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("layouts.global", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /opt/lampp/htdocs/futsal-falad/application/views/dashboard/create.blade.php */ ?>