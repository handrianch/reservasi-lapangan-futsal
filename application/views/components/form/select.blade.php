<label for="{{ $id }}">{{ $label }}</label>
<select name="{{ $name }}" id="{{ $id }}" class="form-control text-capitalize {{ error($name) ? "is-invalid" : "" }}">
  <option value="" disabled selected> --- Pili Salah Satu --- </option>
    @foreach($jenisLapangan as $jlp)
      <option class="text-capitalize" value="{{ $jlp->id }}"
        {{ isset($data->{$name}) ? ($data->{$name} == $jlp->id ? 'selected' : '') : (oldValue($name) == $jlp->id ? 'selected' : '')}}>
        {{ $jlp->nama }}
      </option>
    @endforeach
</select>
{!! error($name) !!}
