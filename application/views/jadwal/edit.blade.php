@extends("layouts.global")

@section("title", "Edit Jadwal")

@section("content")
    <div class="row">
        <div class="col-md-8">
            @component('components.flash.notify')
            @endcomponent

            <form action="{{ base_url("jadwal/$data->id_jadwal_lapangan/update") }}" method="POST" class="shadow-sm p-3 bg-white">
                <div class="form-group">
					@component('components.form.time-picker', [
                        'id' => 'datetimepicker3',
                        'placeholder' => 'Jam',
                        'name' => 'jadwal_jam_lapangan',
                        'label' => 'Jadwal Jam Lapangan',
                        'data' => $data
                    ])

                    @endcomponent
                </div>

                <div class="form-group">
                    {!! getCSRFToken() !!}
                    @component('components.buttons.submit', ['text' => 'Rubah', 'type' => 'primary'])@endcomponent
                    @component('components.buttons.cancel', ['text' => 'Batal', 'type' => 'secondary', 'link' => 'jadwal'])@endcomponent
                </div>
            </form>
        </div>
</div>
@endsection
