<?php

use Jenssegers\Blade\Blade;

if(!function_exists('view')) {
  function view($view, $data = []) {
    $view = str_replace('.', '/', $view);
    $path = APPPATH . 'views';
    $blade = new Blade($path, $path . '/cache');

    echo $blade->make($view, $data);
  }
}

if(!function_exists('error')) {
  function error($name) {
    $instance =& get_instance();
    $instance->load->library('session');
    $errors = $instance->session->flashdata('errors');
    return $errors[$name] ?? '';
  }
}

if(!function_exists('errors')) {
  function errors() {
    $instance =& get_instance();
    $instance->load->library('session');
    return $instance->session->flashdata('errors') ?? '';
  }
}

if(!function_exists('oldValue')) {
  function oldValue($name) {
    $instance =& get_instance();
    $instance->load->library('session');
    $ov = $instance->session->flashdata('oldValue');
    return $ov[$name] ?? '';
  }
}

if(!function_exists('session')) {
  function session($name) {
    $instance =& get_instance();
    $instance->load->library('session');
    return $instance->session->flashdata($name);
  }
}

if(!function_exists('sessionUserData')) {
  function sessionUserData($name) {
    $instance =& get_instance();
    $instance->load->library('session');
    return $instance->session->userdata($name);
  }
}

if(!function_exists('setFlashMessage')) {
  function setFlashMessage(string $message, string $type) : void {
    $instance =& get_instance();
    $instance->session->set_flashdata('flash', true);
    $instance->session->set_flashdata('message', $message);
    $instance->session->set_flashdata('type', $type);
  }
}

if(!function_exists('getCSRFToken')) {
  function getCSRFToken() {
    $instance =& get_instance();
    $csrf = [
        'name' => $instance->security->get_csrf_token_name(),
				'hash' => $instance->security->get_csrf_hash()
    ];

    return '<input type="hidden" name="' . $csrf['name'] .'" value="' . $csrf['hash'] . '">';
  }
}

if(!function_exists('checkAccess')) {
	function checkAccess() {
		$instance =& get_instance();

		return $instance->middleware->checkAccess();
	}
}
