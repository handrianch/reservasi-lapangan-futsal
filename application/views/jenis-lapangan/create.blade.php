@extends("layouts.global")

@section("title", "Tambah Jenis Lapangan")

@section("content")
    <div class="row">
        <div class="col-md-8">
            @component('components.flash.notify')
            @endcomponent

            <form action="{{ base_url('jenis-lapangan') }}" method="POST" class="shadow-sm p-3 bg-white">
                <div class="form-group">
                    @component('components.form.input', [
                        'type' => 'text',
                        'id' => 'nama-jlp',
                        'placeholder' => 'Nama Jenis Lapangan',
                        'name' => 'nama_jenis_lapangan',
                        'label' => 'Nama Jenis Lapangan'
                    ])
                        
                    @endcomponent
                </div>

                <div class="form-group">
                    @component('components.form.input', [
                        'type' => 'number',
                        'id' => 'harga-jlp',
                        'placeholder' => 'Harga',
                        'name' => 'harga',
                        'label' => 'Harga'
                    ])
                        
                    @endcomponent
                </div>

                <div class="form-group">
                    {!! getCSRFToken() !!}
                    @component('components.buttons.submit', ['text' => 'Tambah', 'type' => 'primary'])@endcomponent
                    @component('components.buttons.cancel', ['text' => 'Batal', 'type' => 'secondary', 'link' => 'jenis-lapangan'])@endcomponent
                </div>
            </form>
        </div>
</div>
@endsection