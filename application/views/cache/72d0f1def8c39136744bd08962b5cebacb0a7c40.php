<?php $__env->startSection('title', "List Jenis Lapangan"); ?>

<?php $__env->startSection('pageTitle', 'List Jenis Lapangan'); ?>
<?php $__env->startSection('pageTitleIcon', 'fa-tags'); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <hr class="my-3">
            <div class="row">
                <div class="col-md-12">
                    <?php $__env->startComponent('components.flash.notify'); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
            </div>
            <?php if(checkAccess()): ?>
	            <div class="row mb-3">
	                <div class="col-md-12 text-right">
	                    <?php $__env->startComponent('components.buttons.add', ['link' => 'jenis-lapangan/add', 'text' => 'Tambah Jenis Lapangan']); ?>
	                    <?php echo $__env->renderComponent(); ?>
	                </div>
	            </div>
	        <?php endif; ?>

            <table class="table table-bordered table-stripped">
                <thead>
                    <tr>
                        <th width="10"><strong>No</strong></th>
                        <th><strong>Nama Jenis Lapangan</strong></th>
                        <th><strong>Harga</strong></th>
                        <?php if(checkAccess()): ?>
                        	<th><strong>Aksi</strong></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php $counter = 1; ?>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jlp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td class="text-center"><?php echo e($counter); ?></td>
                            <td class="text-capitalize"><?php echo e($jlp->nama_jenis_lapangan); ?></td>
                            <td>Rp. <?php echo e(number_format($jlp->harga, 0, ",", ".")); ?></td>
                            <?php if(checkAccess()): ?>
	                            <td>
	                                <?php $__env->startComponent('components.buttons.action', [
	                                    'linkUpdate' => "jenis-lapangan/{$jlp->id_jenis_lapangan}/update",
	                                    'linkDelete' => "jenis-lapangan/{$jlp->id_jenis_lapangan}/destroy"
	                                ]); ?>

	                                <?php echo $__env->renderComponent(); ?>
	                            </td>
	                        <?php endif; ?>
                        </tr>
                        <?php $counter++ ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">

                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.global', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /opt/lampp/htdocs/futsal-falad/application/views/jenis-lapangan/index.blade.php */ ?>