<div class="polished-sidebar bg-light col-12 col-md-3 col-lg-2 p-0 collapse d-md-inline" id="sidebar-nav">
    <ul class="polished-sidebar-menu ml-0 pt-4 p-0 d-md-block">
        <li <?php echo e(sessionUserData('active') === 'dashboard' ? 'class=active' : ""); ?>><a href="<?php echo e(base_url()); ?>"><span class="fa fa-home"></span> Dashboard</a></li>
        <li <?php echo e(sessionUserData('active') === 'lapangan' ? 'class=active' : ""); ?>><a href="<?php echo e(base_url("lapangan")); ?>"><span class="fa fa-calendar-o"></span> Manage Lapangan</a></li>
        <li <?php echo e(sessionUserData('active') === 'jenis-lapangan' ? 'class=active' : ""); ?>><a href="<?php echo e(base_url("jenis-lapangan")); ?>"><span class="fa fa-tags"></span> Manage Jenis Lapangan</a></li>
        <li <?php echo e(sessionUserData('active') === 'jadwal' ? 'class=active' : ""); ?>><a href="<?php echo e(base_url("jadwal")); ?>"><span class="fa fa-clock-o"></span> Jadwal</a></li>
        <li <?php echo e(sessionUserData('active') === 'admin' ? 'class=active' : ""); ?>><a href="<?php echo e(base_url("admin")); ?>"><span class="fa fa-users"></span> Manage Admin</a></li>
    </ul>
</div>

<?php /* /opt/lampp/htdocs/futsal-falad/application/views/layouts/components/sidebar.blade.php */ ?>