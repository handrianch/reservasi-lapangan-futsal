<?php
use Models\Model;

class LapanganModel extends Model
{
	protected $table = 'lapangan';
	protected $primaryKey = 'id_lapangan';
	protected $foreignKey = 'id_jenis_lapangan';
	protected $fillable = ['nama_lapangan', 'id_jenis_lapangan'];

	public function getWithRelation(string $relation, bool $type = false , array $orderBy = [])
	{
		if(count($orderBy) <= 0) {
			$orderBy = [$this->primaryKey, 'ASC'];	
		}

		$query = $this->db->select("{$this->table}.id_lapangan, {$this->table}.nama_lapangan, {$relation}.nama_jenis_lapangan")
											->from($this->table)
											->join($relation, "{$this->table}.{$this->foreignKey} = {$relation}.{$this->foreignKey}")
											->order_by(...$orderBy)
											->get();

		if($type) {
			return $query->result_array();
		}

		return $query->result();
	}

	public function getRelation(string $relation)
	{
		return $this->db->select('id_jenis_lapangan as id, nama_jenis_lapangan as nama')->get($relation)->result();
	}

	protected function setUp()
	{
		return ['id_lapangan' => $this->generateID()];
	}

	protected function generateID()
	{
		$oldID = $this->db->select("right(id_lapangan, 2) as kode")
											->order_by('id_lapangan', 'DESC')
											->limit(1)
											->get($this->table)
											->row();
		
		$date = new Datetime('now');
		$y = $date->format('y');
		$m = $date->format('m');
		$d = $date->format('d');

		if($oldID) {
			$newID = intval($oldID->kode) + 1;
			$finalID = "{$y}{$m}{$d}" . str_pad($newID, 2, "0", STR_PAD_LEFT);
			return $finalID;
		}
		
		return "{$y}{$m}{$d}01";
	}
}
