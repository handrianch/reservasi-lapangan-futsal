<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Auth
{
  private $CI = null;
  private $data = null;
  
	public function __construct()
	{
		$this->CI = get_instance();
		$this->CI->load->model('AdminModel', 'AuthModel');
	}
	
	public function attempt($data)
	{
		$this->data = $data;
		
		$user = $this->CI->AuthModel->find($data['username'], 'username');
		
		if(!empty($user)) {
			if(password_verify($data['password'], $user->password)) {
				setFlashMessage('Login successfully', 'success');
				$login = [
						'id' => $user->id_admin,
            'nama' => $user->nama_admin,
            'username' => $user->username,
				];

				$this->CI->session->set_userdata(compact('login'));
				return true;
			}
		}
		
		['message' => $message, 'username' => $username] = $this->getErrMsg();
	
		setFlashMessage($message, 'danger');
		$this->CI->session->set_flashdata('oldValue', compact('username'));
		return false;
	}
	public function logout()
	{
		$this->CI->session->sess_destroy();
		return true;
  }
  
  public function isLogin()
	{
		if(!($this->CI->session->userdata("login"))) {
			setFlashMessage('Login terlebih dulu!', 'danger');
			redirect("login");
    }
	}

	public function isNotLogin()
	{
		if(($this->CI->session->userdata("login"))) {
			redirect('dashboard');
    }
	}

	public function getErrMsg()
	{
		return [
			'username' => $this->data['username'],
			'message' => 'Login gagal!, cek identitas anda',
		];
	}
}