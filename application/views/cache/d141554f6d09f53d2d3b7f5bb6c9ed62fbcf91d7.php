<?php $__env->startSection("title", "Edit Jadwal"); ?>

<?php $__env->startSection("content"); ?>
    <div class="row">
        <div class="col-md-8">
            <?php $__env->startComponent('components.flash.notify'); ?>
            <?php echo $__env->renderComponent(); ?>

            <form action="<?php echo e(base_url("jadwal/$data->id_jadwal_lapangan/update")); ?>" method="POST" class="shadow-sm p-3 bg-white">
                <div class="form-group">
					<?php $__env->startComponent('components.form.time-picker', [
                        'id' => 'datetimepicker3',
                        'placeholder' => 'Jam',
                        'name' => 'jadwal_jam_lapangan',
                        'label' => 'Jadwal Jam Lapangan',
                        'data' => $data
                    ]); ?>

                    <?php echo $__env->renderComponent(); ?>
                </div>

                <div class="form-group">
                    <?php echo getCSRFToken(); ?>

                    <?php $__env->startComponent('components.buttons.submit', ['text' => 'Rubah', 'type' => 'primary']); ?><?php echo $__env->renderComponent(); ?>
                    <?php $__env->startComponent('components.buttons.cancel', ['text' => 'Batal', 'type' => 'secondary', 'link' => 'jadwal']); ?><?php echo $__env->renderComponent(); ?>
                </div>
            </form>
        </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("layouts.global", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /opt/lampp/htdocs/futsal-falad/application/views/jadwal/edit.blade.php */ ?>