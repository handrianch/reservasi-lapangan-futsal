<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ErrorController extends CI_Controller
{
  public function custom404()
  {
    return view('errors.custom.404');
  }

  public function custom403()
  {
    return view('errors.custom.403');
  }
}