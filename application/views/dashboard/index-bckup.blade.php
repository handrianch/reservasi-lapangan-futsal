@extends('layouts.global')

@section('title', "Dashboard")

@section('pageTitle', 'Dashboard')
@section('pageTitleIcon', 'fa-home')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <hr class="my-3">
            <div class="row">
                <div class="col-md-12">
                    @component('components.flash.notify')
                    @endcomponent
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-md-12 text-right">
                    
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <form method="GET" action="">
                        <select name="filter-lapangan" id="filter-lapangan" class="form-control">
                            <option value="" disabled selected>Filter Berdasarkan Lapangan</option>
                            @foreach($lapangan as $lp)
                                <option value="{{ $lp->id_lapangan }}">{{ $lp->nama_lapangan }}</option>
                            @endforeach
                        </select>
                    </form>
                </div>
                <div class="col-md-6">
                    <ul class="nav nav-pills card-header-pills">
                        <li class="nav-item">
                            <a class="nav-link" href="#">All</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Publish</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Draft</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Trash</a>
                        </li>
                    </ul>
                </div>
            </div>
            <hr class="my-3">
            <div class="row mb-3">
                <div class="col-md-12 text-right">
                    <a href="#" class="btn btn-primary">Create Book</a>
                </div>
            </div>
            <table class="table table-bordered table-stripped">
                <thead>
                    <tr>
                        <th><b>Cover</b></th>
                        <th><b>Title</b></th>
                        <th><b>Author</b></th>
                        <th><b>Status</b></th>
                        <th><b>Categories</b></th>
                        <th><b>Stock</b></th>
                        <th><b>Price</b></th>
                        <th><b>Action</b></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>data</td>
                        <td>data</td>
                        <td>data</td>
                        <td><span class="badge bg-dark text-white">status</span></td>
                        <td>
                            <ul class="pl-3">
                                <li>kategori 1</li>
                            </ul>
                        </td>
                        <td>data</td>
                        <td>data</td>
                        <td>
                            @component('components.buttons.action', [
                                'linkUpdate' => "", 
                                'linkDelete' => ""
                            ]) 
                            @endcomponent
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">
                            
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
