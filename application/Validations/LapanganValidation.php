<?php
namespace App\Validations;

class LapanganValidation extends Validation
{
  protected $config = [
    "nama_lapangan" => ['nama_lapangan', 'Nama Lapangan', 'trim|required|min_length[3]|max_length[11]|alpha_numeric_spaces'],
    "id_jenis_lapangan" => ['id_jenis_lapangan', 'Jenis Lapangan', 'required'],
  ];

  public function except()
  {
    $id = $this->instance->uri->segment(2);
    $value = $this->instance->input->post('nama_lapangan');
    $dt = $this->instance->db->select('nama_lapangan')
                             ->where('id_lapangan', $id)
                             ->where('nama_lapangan', $value)
                             ->limit(1)
                             ->get('lapangan');
    
    if(!($dt->num_rows())) {
      $this->config['nama_lapangan'][2] .= "|is_unique[lapangan.nama_lapangan]";
    }
  }

  public function getErrors()
  {
    $errors = [
        'nama_lapangan' => form_error('nama_lapangan'),
        'id_jenis_lapangan' => form_error('id_jenis_lapangan'),
      ];
    
      foreach($errors as $key => $value) {
        if(!($errors[$key])) {
          unset($errors[$key]);
        }
      }

      return $errors;
  }

  public function getOldValue()
  {
    $value = [
      'nama_lapangan' => set_value('nama_lapangan'),
      'id_jenis_lapangan' => set_value('id_jenis_lapangan')
    ];

      return $value;
  }
}