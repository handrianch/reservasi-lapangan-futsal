<label for="{{ $id }}">{{ $label }}</label>
<input type="{{ $type }}" class="form-control {{ error($name) ? "is-invalid" : "" }}" name="{{ $name }}" id="{{ $id }}" 
value="{{ isset($data->{$name}) ? $data->{$name} : oldValue($name) ?: ''}}" placeholder="{{ $placeholder }}">

{!! error($name) !!}