<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="<?php echo e(base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo e(base_url('assets/vendors/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo e(base_url('assets/vendors/nprogress/nprogress.css')); ?>" rel="stylesheet">
    
    <?php echo $__env->yieldContent('style'); ?>

    <!-- Custom Theme Style -->
    <link href="<?php echo e(base_url('assets/build/css/custom.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(base_url('assets/build/css/margin.css')); ?>" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <?php echo $__env->make('layouts.components.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <!-- top navigation -->
        <?php echo $__env->make('layouts.components.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <?php echo $__env->yieldContent('content'); ?>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            <p class="text-mute">Copyright &copy; Palad Futsal <?php echo e((new Datetime('now'))->format('Y')); ?></p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo e(base_url('assets/vendors/jquery/dist/jquery.min.js')); ?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo e(base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo e(base_url('assets/vendors/fastclick/lib/fastclick.js')); ?>"></script>
    <!-- NProgress -->
    <script src="<?php echo e(base_url('assets/vendors/nprogress/nprogress.js')); ?>"></script>
    <!-- Custom Scrollbar -->
    <script src="<?php echo e(base_url('assets/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')); ?>">
    </script>
    <?php echo $__env->yieldContent('script'); ?>
    
    <!-- Custom Theme Scripts -->
    <script src="<?php echo e(base_url('assets/build/js/custom.min.js')); ?>"></script>
  </body>
</html>
<?php /* /opt/lampp/htdocs/futsal-falad/application/views/layouts/master.blade.php */ ?>