@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4 mt-5">
                @component('components.flash.notify')
                @endcomponent

                <div class="card bg-white border shadow-sm">
                    <div class="card-body">
                        <form method="POST" action="{{ base_url('login') }}">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    @component('components.form.input', [
                                        'type' => 'text',
                                        'id' => 'username',
                                        'placeholder' => 'Username',
                                        'name' => 'username',
                                        'label' => 'Username'
                                    ])
                                        
                                    @endcomponent
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    @component('components.form.input', [
                                        'type' => 'password',
                                        'id' => 'password',
                                        'placeholder' => 'Password',
                                        'name' => 'password',
                                        'label' => 'Password'
                                    ])
                                        
                                    @endcomponent
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12">
                                    {!! getCSRFToken() !!}
                                    <button type="submit" class="btn-block btn btn-primary">Login</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div>
                    <hr>
                    <p class="text-center brand text-primary">Palad <i class="fa fa-futbol-o"></i> Futsal</p>
                </div>
            </div>
        </div>
        <div class=" mt-5">
            <div class="col-md-12 text-center">
                <footer>
                    <p class="text-muted fixed-bottom">Copyright &copy; {{ (new Datetime('now'))->format('Y') }}   </p>
                </footer>
            </div>
        </div>
    </div>
@endsection
