<?php $__env->startSection('content'); ?>
    <div class="d-flex flex-row justify-content-center">
        <div class="col-md-6 text-center">
            <div class="alert alert-danger">
                <h1>404</h1>
                <h4>Not Found!! What are you looking for?</h4>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /opt/lampp/htdocs/futsal-falad/application/views/errors/custom/404.blade.php */ ?>