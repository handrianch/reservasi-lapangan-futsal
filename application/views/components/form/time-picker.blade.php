<label for="{{ $id }}">{{ $label }}</label>
<div class="input-group date" id="datetimepicker3" data-target-input="nearest">
	<div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
    <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
	</div>

	<input type="text" class="form-control datetimepicker-input
	{{ error($name) ? "is-invalid" : "" }}" data-target="#datetimepicker3"
	name="{{ $name }}" id="{{ $id }}" value="{{ isset($data->{$name}) ? $data->{$name} : oldValue($name) ?: ''}}" />

	{!! error($name) !!}
</div>

@section('style')
	<link rel="stylesheet" href="{{ base_url('assets/css/tempusdominus-bootstrap-4.min.css') }}">
@endsection

@section('script')
	<script src="{{ base_url('assets/js/moment.js') }}"></script>
	<script src="{{ base_url('assets/js/tempusdominus-bootstrap-4.min.js') }}"></script>
	<script src="{{ base_url('assets/js/custom.js') }}"></script>
@endsection
