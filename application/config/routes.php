<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'DashboardController';
$route['404_override'] = 'ErrorController/custom404';
$route['403_override'] = 'ErrorController/custom403';
$route['translate_uri_dashes'] = FALSE;

$route['dashboard']['get'] = 'DashboardController';

$route['admin']['get'] = 'AdminController/index';
$route['admin/add']['get'] = 'AdminController/create';
$route['admin']['post'] = 'AdminController/store';
$route['admin/(:any)/update']['get'] = 'AdminController/edit/$1';
$route['admin/(:any)/update']['post'] = 'AdminController/update/$1';
$route['admin/(:any)/destroy']['post'] = 'AdminController/destroy/$1';

$route['jenis-lapangan']['get'] = 'JenisLapanganController/index';
$route['jenis-lapangan/add']['get'] = 'JenisLapanganController/create';
$route['jenis-lapangan/(:any)/show']['get'] = 'JenisLapanganController/show/$1';
$route['jenis-lapangan']['post'] = 'JenisLapanganController/store';
$route['jenis-lapangan/(:any)/update']['get'] = 'JenisLapanganController/edit/$1';
$route['jenis-lapangan/(:any)/update']['post'] = 'JenisLapanganController/update/$1';
$route['jenis-lapangan/(:any)/destroy']['post'] = 'JenisLapanganController/destroy/$1';

$route['lapangan']['get'] = 'LapanganController/index';
$route['lapangan/add']['get'] = 'LapanganController/create';
$route['lapangan/(:any)/show']['get'] = 'LapanganController/show/$1';
$route['lapangan']['post'] = 'LapanganController/store';
$route['lapangan/(:any)/update']['get'] = 'LapanganController/edit/$1';
$route['lapangan/(:any)/update']['post'] = 'LapanganController/update/$1';
$route['lapangan/(:any)/destroy']['post'] = 'LapanganController/destroy/$1';

$route['jadwal']['get'] = 'JadwalController/index';
$route['jadwal/add']['get'] = 'JadwalController/create';
$route['jadwal/(:any)/show']['get'] = 'JadwalController/show/$1';
$route['jadwal']['post'] = 'JadwalController/store';
$route['jadwal/(:any)/update']['get'] = 'JadwalController/edit/$1';
$route['jadwal/(:any)/update']['post'] = 'JadwalController/update/$1';
$route['jadwal/(:any)/destroy']['post'] = 'JadwalController/destroy/$1';

$route['sewa/add']['get'] = 'DashboardController/create';

$route['login']['get'] = 'AuthController/login';
$route['login']['post'] = 'AuthController/attempt';
$route['logout']['post'] = 'AuthController/logout';

$route['(.*)'] = 'none';
