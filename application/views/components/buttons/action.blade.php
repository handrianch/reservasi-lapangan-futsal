<a href="{{ base_url($linkUpdate) }}" class="btn btn-sm btn-info"><span class="fa fa-edit"></span> Edit</a>
<form class="d-inline" method="POST" action="{{ base_url($linkDelete) }}"
    onsubmit="return confirm('Yakin untuk menghapus?')">
    {!! getCSRFToken() !!}
    <button class="btn btn-danger btn-sm" type="submit"><span class="fa fa-trash"></span> Hapus</button>
</form>