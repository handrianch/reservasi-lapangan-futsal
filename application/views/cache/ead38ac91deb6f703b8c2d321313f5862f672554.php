<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9 no-js" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Palad Futsal</title>
		<link rel="stylesheet" href="<?php echo e(base_url('assets/css/iconic/css/open-iconic-bootstrap.min.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(base_url('assets/css/polished.min.css')); ?> ">
		<link rel="stylesheet" href="<?php echo e(base_url('assets/css/custom.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(base_url('assets/vendors/font-awesome/css/font-awesome.min.css')); ?>">
		<style>

		</style>
		<script type="text/javascript">
			document.documentElement.className = document.documentElement.className.replace('no-js', 'js') + (document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1") ? ' svg' : ' no-svg');
		</script>
	</head>
	<body>
		<nav class="navbar navbar-expand p-0">
			<a class="navbar-brand text-center col-xs-12 col-md-3 col-lg-2 mr-0" href="<?php echo e(base_url()); ?>"> Palad <i class="fa fa-futbol-o"></i> Futsal </a>
			<button class="btn btn-link d-block d-md-none" data-toggle="collapse" data-target="#sidebar-nav" role="button" >
				<span class="oi oi-menu"></span>
			</button>
		</nav>

		<div class="container-fluid p-0">
			<div style="min-height: 100%" class="flex-row d-flex align-itemsstretch m-0">
				<div class="col-lg-12 col-md-12 p-4">
				  <?php echo $__env->yieldContent('content'); ?>
				</div>
			</div>
		</div>

		<script src="<?php echo e(base_url("assets/js/jquery-3.3.1.min.js")); ?>"></script>
		<script src="<?php echo e(base_url("assets/js/popper.min.js")); ?>""></script>
		<script src="<?php echo e(base_url("assets/js/bootstrap.min.js")); ?>""></script>
	</body>
</html>

<?php /* /opt/lampp/htdocs/futsal-falad/application/views/layouts/app.blade.php */ ?>