<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Form testing</title>
</head>
<body>
  <form action="<?php echo e(base_url('jenis-lapangan')); ?>" method="POST">
    <label for="">Nama Jenis Lapangan</label>
    <input type="text" name="nama" value="<?php echo e(oldValue('nama')); ?>">
    <?php echo error('nama'); ?>

    <br>

    <label for="">Harga Lapangan</label>
    <input type="text" name="harga">
    <?php echo error('harga'); ?>

    <br>
    <input type="hidden" name="<?php echo e($csrf['name']); ?>" value="<?php echo e($csrf['hash']); ?>" >
    <button>submit</button>
  </form>
</body>
</html>
<?php /* /opt/lampp/htdocs/futsal-falad/application/views/form_test.blade.php */ ?>