<?php defined('BASEPATH') or exit('No direct script access allowed');

class FormValidation
{
  private $instance;

  public function __construct()
  {
    $this->instance =& get_instance();
    $this->instance->load->library('form_validation');
  }

  public function validate($class)
  {
    $arrClass = explode("_", $class);
    $newArrClass = array_map(function($arr) {
      return ucfirst($arr);
    }, $arrClass);
    $class = "App\\Validations\\" . implode("", $newArrClass) . "Validation";

    return (new $class)->run($this->instance);
  }

  public function load($class)
  {
  	$arrClass = explode("_", $class);
    $newArrClass = array_map(function($arr) {
      return ucfirst($arr);
    }, $arrClass);
    $class = "App\\Validations\\" . implode("", $newArrClass) . "Validation";

  	return new $class($this->instance);
  }
}
