<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <ul class="nav navbar-nav navbar-right">
        <li class="">
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            John Doe <span class=" fa fa-angle-down"></span>
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li>
              <form action="<?php echo e(base_url('logout')); ?>" method="POST" class="d-inline">
                <a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
              </form>
            </li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>
</div>
<?php /* /opt/lampp/htdocs/futsal-falad/application/views/layouts/components/navbar.blade.php */ ?>