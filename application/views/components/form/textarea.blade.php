<label for="{{ $id }}">{{ $label }}</label>
<textarea class="form-control {{ error($name) ? "is-invalid" : "" }}" name="{{ $name }}" id="{{ $id }}" cols="30" rows="5" placeholder="Tulis alamat penyewa"></textarea>

{!! error($name) !!}