<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4 mt-5">
                <?php $__env->startComponent('components.flash.notify'); ?>
                <?php echo $__env->renderComponent(); ?>

                <div class="card bg-white border shadow-sm">
                    <div class="card-body">
                        <form method="POST" action="<?php echo e(base_url('login')); ?>">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <?php $__env->startComponent('components.form.input', [
                                        'type' => 'text',
                                        'id' => 'username',
                                        'placeholder' => 'Username',
                                        'name' => 'username',
                                        'label' => 'Username'
                                    ]); ?>
                                        
                                    <?php echo $__env->renderComponent(); ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <?php $__env->startComponent('components.form.input', [
                                        'type' => 'password',
                                        'id' => 'password',
                                        'placeholder' => 'Password',
                                        'name' => 'password',
                                        'label' => 'Password'
                                    ]); ?>
                                        
                                    <?php echo $__env->renderComponent(); ?>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12">
                                    <?php echo getCSRFToken(); ?>

                                    <button type="submit" class="btn-block btn btn-primary">Login</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div>
                    <hr>
                    <p class="text-center brand text-primary">Palad <i class="fa fa-futbol-o"></i> Futsal</p>
                </div>
            </div>
        </div>
        <div class=" mt-5">
            <div class="col-md-12 text-center">
                <footer>
                    <p class="text-muted fixed-bottom">Copyright &copy; <?php echo e((new Datetime('now'))->format('Y')); ?>   </p>
                </footer>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /opt/lampp/htdocs/futsal-falad/application/views/auth/login.blade.php */ ?>