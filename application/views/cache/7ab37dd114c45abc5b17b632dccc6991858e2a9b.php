<?php if(session('flash')): ?>
    <?php $__env->startComponent('components.flash.alert', ['type' => session('type')]); ?>
        <?php echo e(session('message')); ?>

    <?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php /* /opt/lampp/htdocs/futsal-falad/application/views/components/flash/notify.blade.php */ ?>