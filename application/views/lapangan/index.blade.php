@extends('layouts.global')

@section('title', "List Lapangan")

@section('pageTitle', 'List Lapangan')
@section('pageTitleIcon', 'fa-calendar-o')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <hr class="my-3">
            <div class="row">
                <div class="col-md-12">
                    @component('components.flash.notify')
                    @endcomponent
                </div>
            </div>
            @if(checkAccess())
	            <div class="row mb-3">
	                <div class="col-md-12 text-right">
	                    @component('components.buttons.add', ['link' => 'lapangan/add', 'text' => 'Tambah Lapangan'])
	                    @endcomponent
	                </div>
	            </div>
	         @endif

            <table class="table table-bordered table-stripped">
                <thead>
                    <tr>
                        <th width="10"><strong>No</strong></th>
                        <th><strong>Nama Lapangan</strong></th>
                        <th><strong>Jenis Lapangan</strong></th>
                        @if(checkAccess())
                        	<th><strong>Aksi</strong></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @php $counter = 1; @endphp
                    @foreach($data as $lp)
                        <tr>
                            <td class="text-center">{{ $counter }}</td>
                            <td class="text-capitalize">{{$lp->nama_lapangan}}</td>
                            <td class="text-capitalize"><strong>{{ $lp->nama_jenis_lapangan }}</strong></td>
                            @if(checkAccess())
                            <td>
                                @component('components.buttons.action', [
                                    'linkUpdate' => "lapangan/{$lp->id_lapangan}/update",
                                    'linkDelete' => "lapangan/{$lp->id_lapangan}/destroy"
                                ])

                                @endcomponent
                            </td>
                            @endif
                        </tr>
                        @php $counter++ @endphp
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">

                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
