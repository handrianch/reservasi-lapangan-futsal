@extends('layouts.global')

@section('title', "List Admin")

@section('pageTitle', 'List Admin')
@section('pageTitleIcon', 'fa-users')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <hr class="my-3">
            <div class="row">
                <div class="col-md-12">
                    @component('components.flash.notify')
                    @endcomponent
                </div>
            </div>
            @if(checkAccess())
	            <div class="row mb-3">
	                <div class="col-md-12 text-right">
	                    @component('components.buttons.add', ['link' => 'admin/add', 'text' => 'Tambah Admin'])
	                    @endcomponent
	                </div>
	            </div>
	         @endif

            <table class="table table-bordered table-stripped">
                <thead>
                    <tr>
                        <th width="10"><strong>No</strong></th>
                        <th><strong>Nama</strong></th>
                        <th><strong>Username</strong></th>
                        @if(checkAccess())
                        	<th><strong>Aksi</strong></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @php $counter = 1; @endphp
                    @foreach($data as $adm)
                        <tr>
                            <td class="text-center">{{ $counter }}</td>
                            <td class="text-capitalize">{{$adm->nama_admin}}</td>
                            <td><strong>{{ $adm->username }}</strong></td>
                            @if(checkAccess())
	                            <td>
	                                @component('components.buttons.action', [
	                                    'linkUpdate' => "admin/{$adm->id_admin}/update",
	                                    'linkDelete' => "admin/{$adm->id_admin}/destroy"
	                                ])
	                                @endcomponent
	                            </td>
	                        @endif
                        </tr>
                        @php $counter++ @endphp
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">

                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
