<?php
namespace App\Validations;

class UserValidation extends Validation
{
  protected $config = [
    "username" => ['username', 'Username', 'required'],
    "password" => ['password', 'Password', 'required'],
  ];

  public function except()
  {
    // skip
  }

  public function getErrors()
  {
    $errors = [
        'username' => form_error('username'),
        'password' => form_error('password'),
      ];
    
      foreach($errors as $key => $value) {
        if(!($errors[$key])) {
          unset($errors[$key]);
        }
      }

      return $errors;
  }

  public function getOldValue()
  {
    return [
      'username' => set_value('username')
    ];
  }
}