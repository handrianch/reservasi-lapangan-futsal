@if(session('flash'))
    @component('components.flash.alert', ['type' => session('type')])
        {{ session('message') }}
    @endcomponent
@endif