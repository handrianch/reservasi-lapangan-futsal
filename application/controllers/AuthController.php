<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AuthController extends CI_Controller
{
  public function login()
  {
    $this->auth->isNotLogin();
    return view('auth.login');
  }

  public function attempt()
  {
    $this->auth->isNotLogin();

    $this->load->library('FormValidation');
    
    if(!($this->formvalidation->validate('user'))) {
      setFlashMessage('Login gagal!, cek identitas anda', 'danger');
      redirect(base_url("login"));
    }

    $data = [
      'username' => $this->input->post('username', true),
      'password' => $this->input->post('password', true),
    ];

    if($this->auth->attempt($data)){
      redirect(base_url('dashboard'));
    }
    
    setFlashMessage(session('message'), 'danger');
    redirect(base_url("login"));
  }

  public function logout()
  {
    $this->auth->isLogin();
    $this->auth->logout();

    redirect(base_url());
  }
}