<?php
namespace App\Validations;

class JenisLapanganValidation extends Validation
{
  protected $config = [
    "nama_jenis_lapangan" => ['nama_jenis_lapangan', 'Nama', 'trim|required|min_length[3]|max_length[10]|alpha'],
    "harga" => ['harga', 'Harga', 'trim|required|min_length[5]|max_length[6]|numeric'],
  ];

  public function except()
  {
    $id = $this->instance->uri->segment(2);
    $value = $this->instance->input->post('nama_jenis_lapangan');
    $dt = $this->instance->db->select('nama_jenis_lapangan')
                             ->where('id_jenis_lapangan', $id)
                             ->where('nama_jenis_lapangan', $value)
                             ->limit(1)
                             ->get('jenis_lapangan');
    
    if(!($dt->num_rows())) {
      $this->config['nama_jenis_lapangan'][2] .= "|is_unique[jenis_lapangan.nama_jenis_lapangan]";
    }
  }

  public function getErrors()
  {
    $errors = [
        'nama_jenis_lapangan' => form_error('nama_jenis_lapangan'),
        'harga' => form_error('harga'),
      ];
    
      foreach($errors as $key => $value) {
        if(!($errors[$key])) {
          unset($errors[$key]);
        }
      }

      return $errors;
  }

  public function getOldValue()
  {
    $value = [
      'nama_jenis_lapangan' => set_value('nama_jenis_lapangan'),
      'harga' => set_value('harga')
    ];

      return $value;
  }
}