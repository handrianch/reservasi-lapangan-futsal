@extends('layouts.global')

@section('title', "List Jadwal")

@section('pageTitle', 'List Jadwal Jam')
@section('pageTitleIcon', 'fa-clock-o')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <hr class="my-3">
            <div class="row">
                <div class="col-md-12">
                    @component('components.flash.notify')
                    @endcomponent
                </div>
            </div>
            @if(checkAccess())
	            <div class="row mb-3">
	                <div class="col-md-12 text-right">
	                    @component('components.buttons.add', ['link' => 'jadwal/add', 'text' => 'Tambah Jadwal'])
	                    @endcomponent
	                </div>
	            </div>
	        @endif

            <table class="table table-bordered table-stripped">
                <thead>
                    <tr>
                        <th width="10"><strong>No</strong></th>
                        <th><strong>Jam</strong></th>
                        @if(checkAccess())
                        	<th><strong>Aksi</strong></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @php $counter = 1; @endphp
                    @foreach($data as $jam)
                        <tr>
                            <td class="text-center">{{ $counter }}</td>
                            <td>{{$jam->jadwal_jam_lapangan}}</td>
                            @if(checkAccess())
	                            <td>
	                                @component('components.buttons.action', [
	                                    'linkUpdate' => "jadwal/{$jam->id_jadwal_lapangan}/update",
	                                    'linkDelete' => "jadwal/{$jam->id_jadwal_lapangan}/destroy"
	                                ])
	                                @endcomponent
	                            </td>
	                        @endif
                        </tr>
                        @php $counter++ @endphp
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">

                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
