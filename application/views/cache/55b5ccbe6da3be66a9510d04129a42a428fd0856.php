<label for="<?php echo e($id); ?>"><?php echo e($label); ?></label>
<select name="<?php echo e($name); ?>" id="<?php echo e($id); ?>" class="form-control text-capitalize <?php echo e(error($name) ? "is-invalid" : ""); ?>">
  <option value="" disabled selected> --- Pili Salah Satu --- </option>
    <?php $__currentLoopData = $jenisLapangan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jlp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <option class="text-capitalize" value="<?php echo e($jlp->id); ?>"
        <?php echo e(isset($data->{$name}) ? ($data->{$name} == $jlp->id ? 'selected' : '') : (oldValue($name) == $jlp->id ? 'selected' : '')); ?>>
        <?php echo e($jlp->nama); ?>

      </option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select>
<?php echo error($name); ?>


<?php /* /opt/lampp/htdocs/futsal-falad/application/views/components/form/select.blade.php */ ?>