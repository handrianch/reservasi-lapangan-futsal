<div class="alert alert-{{ $type }} text-center text-small">
    {{ $slot }}
</div>