@extends("layouts.global")

@section("title", "Tambah Jadwal")

@section("content")
    <div class="row">
        <div class="col-md-8">
            @component('components.flash.notify')
            @endcomponent

            <form action="{{ base_url('jadwal') }}" method="POST" class="shadow-sm p-3 bg-white">
                <div class="form-group">
					@component('components.form.time-picker', [
                        'id' => 'datetimepicker3',
                        'placeholder' => 'Jam',
                        'name' => 'jadwal_jam_lapangan',
                        'label' => 'Jadwal Jam Lapangan'
                    ])

                    @endcomponent
                </div>

                <div class="form-group">
                    {!! getCSRFToken() !!}
                    @component('components.buttons.submit', ['text' => 'Tambah', 'type' => 'primary'])@endcomponent
                    @component('components.buttons.cancel', ['text' => 'Batal', 'type' => 'secondary', 'link' => 'jadwal'])@endcomponent
                </div>
            </form>
        </div>
</div>
@endsection
