<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->auth->isLogin();
		$this->middleware->protect(["*"]);
		$this->load->model('AdminModel', 'Model');
		$this->session->set_userdata('active', 'admin');
	}

	/**
	 * Get all data from db
	 * @return  mixed
	 */
	public function index()
	{
		$data = $this->Model->get(false, ["right(id_admin, 1) ASC"]);

		foreach($data as $d) {
			unset($d->password);
		}

		return view('admin.index', compact('data'));
	}

	/**
	 * rendering view for form to create data
	 * @return  mixed
	 */
	public function create()
	{
		return view('admin.create');
	}

	/**
	 * storing data to database from request
	 * @return  mixed
	 */
	public function store()
	{
		// storing data from request to database
		$this->load->library('FormValidation');

		if(!($this->formvalidation->validate('admin'))) {
			setFlashMessage('Data gagal dibuat', 'danger');
			redirect(base_url("admin/add"));
		}

		$data = [];

		foreach($this->input->post(null, true) as $key => $input) {
			if(strtoupper($key) === strtoupper('password') || strtoupper($key) === strtoupper('re-password')) {
				$data[$key] = $input;
			} else {
				$data[$key] = strtolower($input);
			}
		}


		$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
		$result = $this->Model->store($data);

		if(!$result) {
			setFlashMessage('Data gagal ditambahkan', 'danger');
			redirect(base_url("admin/add"));
		}

		setFlashMessage('Data bershasil ditambahkan', 'success');
		redirect(base_url("admin"));
	}

	/**
	 * rendering view for form editing data
	 * @param  string $id
	 * @return  mixed
	 */
	public function edit(string $id)
	{
		// render view for edit existing data
		$data = $this->Model->find($id);
		unset($data->password);

		return view('admin.edit', compact('data'));
	}

	/**
	 * storing updated data to database from request
	 * @param  string $id
	 * @return  mixed
	 */
	public function update(string $id)
	{
		// storing data from request to database
		$this->load->library('FormValidation');

		if(!($this->formvalidation->validate('admin'))) {
			setFlashMessage('Data gagal diupdate', 'danger');
			redirect(base_url("admin/{$id}/update"));
		}

		$data = $this->input->post(null, true);

		if(empty($data['password']) && empty($data['re-password'])) {
			$data['password'] = ($this->Model->find($id))->password;
    } else {
			$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
		}

		$result = $this->Model->update($data, $id);

		if(!$result) {
			setFlashMessage('Data gagal diupdate', 'danger');
			redirect(base_url("admin/{$id}/update"));
		}

		setFlashMessage('Data bershasil diupdate', 'success');
		redirect(base_url("admin"));
	}

	/**
	 * deleting data from database
	 * @param  string $id
	 * @return  mixed
	 */
	public function destroy(string $id)
	{
		// destroy data from database
		$result = $this->Model->destroy($id);

		if(!$result) {
			setFlashMessage('Data gagal di hapus', 'danger');
			redirect(base_url("admin"));
		}

		setFlashMessage('Data bershasil di hapus', 'success');
		redirect(base_url("admin"));
	}
}
