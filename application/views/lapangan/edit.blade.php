@extends("layouts.global")

@section("title", "Tambah Lapangan")

@section("content")
    <div class="row">
        <div class="col-md-8">
            @component('components.flash.notify')
            @endcomponent

            <form action="{{ base_url("lapangan/{$data->id_lapangan}/update") }}" method="POST" class="shadow-sm p-3 bg-white">
                <div class="form-group">
                    @component('components.form.input', [
                        'type' => 'text',
                        'id' => 'nama-lp',
                        'placeholder' => 'Nama Lapangan',
                        'name' => 'nama_lapangan',
                        'label' => 'Nama Lapangan',
                        'data' => $data
                    ])
                        
                    @endcomponent
                </div>

                <div class="form-group">
                   @component('components.form.select', [
                       'id' => 'jlp',
                       'label' => 'Jenis Lapangan',
                       'name' => 'id_jenis_lapangan',
                       'data' => $data,
                       'jenisLapangan' => $jenisLapangan
                    ])
                        
                    @endcomponent
                </div>

                <div class="form-group">
                    {!! getCSRFToken() !!}
                    @component('components.buttons.submit', ['text' => 'Rubah', 'type' => 'primary'])@endcomponent
                    @component('components.buttons.cancel', ['text' => 'Batal', 'type' => 'secondary', 'link' => 'lapangan'])@endcomponent
                </div>
            </form>
        </div>
</div>
@endsection