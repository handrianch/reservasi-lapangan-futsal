<?php

use Models\Model;

class JenisLapanganModel extends Model
{
	protected $table = 'jenis_lapangan';
	protected $primaryKey = 'id_jenis_lapangan';
	protected $fillable = ['nama_jenis_lapangan', 'harga'];

	protected function setUp()
	{
		return ['id_jenis_lapangan' => $this->generateID()];
	}

	protected function generateID()
	{
		$oldID = $this->db->select("right($this->primaryKey, 1) as kode")
											->order_by($this->primaryKey, 'DESC')
											->limit(1)
											->get($this->table)
											->row();
		
		$date = new Datetime('now');
		$y = $date->format('y');
		$m = $date->format('m');
		$d = $date->format('d');

		if($oldID) {
			$newID = intval($oldID->kode) + 1;
			$finalID = "{$y}{$m}{$d}{$newID}";	
			return $finalID;
		}
		
		$format = "{$y}{$m}{$d}1";
		return $format;
	}
}
