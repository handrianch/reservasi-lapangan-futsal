<?php
namespace App\Validations;

class JadwalValidation extends Validation
{
	public function __construct($instance = null)
	{
		// parent::__construct();
		$this->instance = $instance;
	}

  protected $config = [
    "jadwal_jam_lapangan" => ['jadwal_jam_lapangan', 'Jadwal Jam Lapangan', 'trim|required']
  ];

  public function except()
  {
    $id = $this->instance->uri->segment(2);
    $value = $this->instance->input->post('jadwal_jam_lapangan');

    $dt = $this->instance->db->select('*')
    												 ->from('jadwal_lapangan')
    												 ->where('id_jadwal_lapangan', $id)
    												 ->where('jadwal_jam_lapangan', $value)
    												 ->limit(1)
    												 ->get();

    if(!($dt->num_rows())) {
      $this->config['jadwal_jam_lapangan'][2] .= "|is_unique[jadwal_lapangan.jadwal_jam_lapangan]";
    }
  }

  public function checkTimeFormat($data)
  {
  	if(preg_match('/^[0-9]{2}:[0-9]{2}$/', $data['jadwal_jam_lapangan'])) {
			return true;
		}

		$jadwal_jam_lapangan = "<div class='invalid-feedback'>Format jam tidak diketahui</div>";
    $this->instance->session->set_flashdata('errors', compact('jadwal_jam_lapangan'));

		return false;
  }

  public function getErrors()
  {
    $errors = [
        'jadwal_jam_lapangan' => form_error('jadwal_jam_lapangan')
      ];

    foreach($errors as $key => $value) {
      if(!($errors[$key])) {
        unset($errors[$key]);
      }
    }

    return $errors;
  }

  public function getOldValue()
  {
    $value = [
      'jadwal_jam_lapangan' => set_value('jadwal_jam_lapangan')
    ];

    return $value;
  }
}
