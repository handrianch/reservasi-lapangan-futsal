<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LapanganController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->auth->isLogin();
		$this->middleware->protect(['create', 'edit', 'store', 'update', 'destroy']);
		$this->load->model('LapanganModel', 'Model');
		$this->session->set_userdata('active', 'lapangan');
	}

	/**
	 * Get all data from db
	 * @return  mixed
	 */
	public function index()
	{
		$data = $this->Model->getWithRelation('jenis_lapangan');

		return view('lapangan.index', compact('data'));
	}

	/**
	 * rendering view for form to create data
	 * @return  mixed
	 */
	public function create()
	{
		// render view form for creating jenis lapangan
		$jenisLapangan = $this->Model->getRelation('jenis_lapangan');

		return view('lapangan.create', compact('jenisLapangan'));
	}

	/**
	 * storing data to database from request
	 * @return  mixed
	 */
	public function store()
	{
		// storing data from request to database
		$this->load->library('FormValidation');

		if(!($this->formvalidation->validate('lapangan'))) {
			setFlashMessage('Data gagal ditambahkan', 'danger');
			redirect(base_url("lapangan/add"));
		}

		$data = [];

		foreach($this->input->post(null, true) as $key => $value) {
			$data[$key] = strtolower($value);
		}

		$result = $this->Model->store($data);

		if(!$result) {
			setFlashMessage('Data gagal ditambahkan', 'danger');
			redirect(base_url("lapangan/add"));
		}

		setFlashMessage('Data bershasil ditambahkan', 'success');
		redirect(base_url("lapangan"));
	}

	/**
	 * rendering view for form editing data
	 * @param  string $id
	 * @return  mixed
	 */
	public function edit(string $id)
	{
		// render view for edit existing data
		$data = $this->Model->find($id);
		$jenisLapangan = $this->Model->getRelation('jenis_lapangan');

		return view('lapangan.edit', compact('data', 'jenisLapangan'));
	}

	/**
	 * storing updated data to database from request
	 * @param  string $id
	 * @return  mixed
	 */
	public function update(string $id)
	{
		// storing data from request to database
		$this->load->library('FormValidation');

		if(!($this->formvalidation->validate('lapangan'))) {
			setFlashMessage('Data gagal diupdate', 'danger');
			redirect(base_url("lapangan/{$id}/update"));
		}


		$result = $this->Model->update($this->input->post(null, true), $id);

		if(!$result) {
			setFlashMessage('Data gagal diupdate', 'danger');
			redirect(base_url("lapangan/{$id}/update"));
		}

		setFlashMessage('Data bershasil diupdate', 'success');
		redirect(base_url("lapangan"));

	}

	/**
	 * deleting data from database
	 * @param  string $id
	 * @return  mixed
	 */
	public function destroy(string $id)
	{
		// destroy data from database
		$result = $this->Model->destroy($id);

		if(!$result) {
			setFlashMessage('Data gagal di hapus', 'danger');
			redirect(base_url("lapangan/{$id}/show"));
		}

		setFlashMessage('Data bershasil di hapus', 'success');
		redirect(base_url("lapangan"));
	}
}
