<?php
use Models\Model;

class AdminModel extends Model
{
  protected $table = 'admin';
	protected $primaryKey = 'id_admin';
	protected $fillable = ['nama_admin', 'username', 'password'];

	protected function setUp()
	{
		return [
			'id_admin' 	=> $this->generateID()
		];
	}

	protected function generateID()
	{
		$tipeAkun = $this->input->post('tipe-akun', true);

		if(strtoupper($tipeAkun) === "SU") {
			$akun = "SU";
		} else {
			$akun = "AD";
		}

		$oldID = $this->db->select("right($this->primaryKey, 1) as kode")
											->order_by('right(id_admin, 1) DESC')
											->limit(1)
											->get($this->table)
											->row();

		if($oldID) {
			$newID = intval($oldID->kode) + 1;
			$finalID = $akun . $newID;
			return $finalID;
		}

		return "USR01";
	}
}
