<?php
namespace Models;

abstract class Model extends \CI_Model
{	
	abstract protected function generateID();
	abstract protected function setUp();

	public function get(bool $type = false, array $orderBy = [])
	{
		if(count($orderBy) <= 0) {
			$orderBy = [$this->primaryKey, 'ASC'];	
		}

		$query = $this->db->order_by(...$orderBy)->get($this->table);

		if($type) {
			return $query->result_array();
		}
		
		return $query->result();
	}

	public function store(array $data)
	{
		$finalData = $this->setup();
		
		foreach($this->fillable as $fill) {
			$finalData[$fill] = $data[$fill];
		}
		
		return $this->db->insert($this->table, $finalData);
	}

	public function update($data, $value, $field = '')
	{
		$field = $field ?: $this->primaryKey;
		$obj = $this->find($value, $field);
		$finalData = [];

		foreach($this->fillable as $fill) {
			$finalData[$fill] = $data[$fill];
		}

		return $obj->update($this->table, $finalData);
	}

	public function destroy($value, $field = '')
	{
		$field = $field ?: $this->primaryKey;
		$dt = $this->find($value, $field)->get($this->table)->num_rows();
		if(!($dt)) {
			return false;
		}
		return $this->find($value, $field)->delete($this->table);
	}

	private function find(string $value = '', string $field = '')
	{
		$field = $field ?: $this->primaryKey;
		return $this->db->where($field, $value);
	}
	
	public function __call($method, $params)
	{
		if($method === 'find') {
			return $this->find(...$params)->get($this->table)->row();
		}
	}
}
