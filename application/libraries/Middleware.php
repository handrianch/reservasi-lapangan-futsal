<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Middleware
{
	private $instance = null;
	private $method = null;
	private $params = [];

	public function __construct()
	{
		$this->instance =& get_instance();
		$this->method = $this->instance->router->fetch_method();
		$this->session = $this->instance->session->userdata('login');
		$this->akunType = substr($this->session['id'], 0, 2);
	}

	public function protect(array $params)
	{
		$this->params = $params;

		if(!$this->check()) {
			setFlashMessage('Akses Ditolak', 'danger');
			return redirect($_SERVER['HTTP_REFERER']);
		}

		return false;
	}

	public function checkAccess()
	{
		return strtoupper($this->akunType) === 'SU';
	}

	private function check()
	{
		if(strtoupper($this->akunType) === 'SU') {
			return true;
		} elseif(in_array($this->method, $this->params) || $this->params[0] == "*") {
			return false;
		}

		return true;
	}
}


// 1. cek apakah dia login sebagai SU
		// jika YA maka boleh akses kesemua nya
// 2. cek apakah method tersebut di protect
		// jika YA
