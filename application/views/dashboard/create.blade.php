@extends("layouts.global")

@section("title", "Tambah Admin")

@section("content")
    <div class="row">
        <div class="col-md-8">
            @component('components.flash.notify')
            @endcomponent

            <form action="{{ base_url('admin') }}" method="POST" class="shadow-sm p-3 bg-white">
                <div class="form-group">
                    @component('components.form.input', [
                        'type' => 'text',
                        'id' => 'nama-penyewa',
                        'placeholder' => 'Nama Penyewa',
                        'name' => 'nama_penyewa',
                        'label' => 'Nama Penyewa'
                    ])
                        
                    @endcomponent
                </div>

                <div class="form-group">
                    @component('components.form.textarea', [
                        'id' => 'alamat-penyewa',
                        'placeholder' => 'Alamat Penyewa',
                        'name' => 'alamat_penyewa',
                        'label' => 'Alamat Penyewa'
                    ])
                        
                    @endcomponent
                </div>

                <div class="form-group">
                    <label for="">Pilih Lapangan</label>
                    <select name="" id="" class="form-control">
                        <option value="">Lapangan 1</option>
                    </select>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            @component('components.form.input', [
                                'type' => 'hours',
                                'id' => 'lama-sewa',
                                'placeholder' => 'Lama Sewa',
                                'name' => 'lama-sewa',
                                'label' => 'Lama Sewa'
                            ])
                        
                            @endcomponent
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Mulai Jam : </label>
                            <select name="" id="" class="form-control">
                                <option value="">08 : 00</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Status Bayar : </label> <br />
                    
                    @component('components.form.radio', [
                        'label' => 'Cash',
                        'id' => 'adm',
                        'name' => 'tipe-akun',
                        'value' => 'adm',
                        'checked' => 'checked'
                    ])@endcomponent

                    @component('components.form.radio', [
                        'label' => 'On Proses',
                        'id' => 'su',
                        'name' => 'tipe-akun',
                        'value' => 'su',
                        'checked' => ''
                    ])@endcomponent

                    @component('components.form.radio', [
                        'label' => 'DP',
                        'id' => 'su',
                        'name' => 'tipe-akun',
                        'value' => 'su',
                        'checked' => ''
                    ])@endcomponent
                </div>

                <div class="form-group">
                    {!! getCSRFToken() !!}
                    @component('components.buttons.submit', ['text' => 'Tambah', 'type' => 'primary'])@endcomponent
                    @component('components.buttons.cancel', ['text' => 'Batal', 'type' => 'secondary', 'link' => 'admin'])@endcomponent
                </div>
            </form>
        </div>
</div>
@endsection