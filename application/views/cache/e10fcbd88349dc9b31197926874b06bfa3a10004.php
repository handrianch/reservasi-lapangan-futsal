<?php $__env->startSection('title', "List Lapangan"); ?>

<?php $__env->startSection('pageTitle', 'List Lapangan'); ?>
<?php $__env->startSection('pageTitleIcon', 'fa-calendar-o'); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <hr class="my-3">
            <div class="row">
                <div class="col-md-12">
                    <?php $__env->startComponent('components.flash.notify'); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
            </div>
            <?php if(checkAccess()): ?>
	            <div class="row mb-3">
	                <div class="col-md-12 text-right">
	                    <?php $__env->startComponent('components.buttons.add', ['link' => 'lapangan/add', 'text' => 'Tambah Lapangan']); ?>
	                    <?php echo $__env->renderComponent(); ?>
	                </div>
	            </div>
	         <?php endif; ?>

            <table class="table table-bordered table-stripped">
                <thead>
                    <tr>
                        <th width="10"><strong>No</strong></th>
                        <th><strong>Nama Lapangan</strong></th>
                        <th><strong>Jenis Lapangan</strong></th>
                        <?php if(checkAccess()): ?>
                        	<th><strong>Aksi</strong></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php $counter = 1; ?>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td class="text-center"><?php echo e($counter); ?></td>
                            <td class="text-capitalize"><?php echo e($lp->nama_lapangan); ?></td>
                            <td class="text-capitalize"><strong><?php echo e($lp->nama_jenis_lapangan); ?></strong></td>
                            <?php if(checkAccess()): ?>
                            <td>
                                <?php $__env->startComponent('components.buttons.action', [
                                    'linkUpdate' => "lapangan/{$lp->id_lapangan}/update",
                                    'linkDelete' => "lapangan/{$lp->id_lapangan}/destroy"
                                ]); ?>

                                <?php echo $__env->renderComponent(); ?>
                            </td>
                            <?php endif; ?>
                        </tr>
                        <?php $counter++ ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">

                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.global', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /opt/lampp/htdocs/futsal-falad/application/views/lapangan/index.blade.php */ ?>