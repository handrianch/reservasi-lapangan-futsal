<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DashboardController extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->auth->isLogin();
    $this->session->set_userdata('active', 'dashboard');
  }

  public function index()
  {
    $lapangan = $this->getLapangan();
    $jadwal = $this->getJadwal();
    return view('dashboard.index', compact('lapangan', 'jadwal'));
  }

  public function create()
  {
    return view('dashboard.create');
  }

  private function getLapangan()
  {
    $this->load->model('LapanganModel');
    return $this->LapanganModel->get(false, ['id_lapangan', 'ASC']);
  }

  private function getJadwal()
  {
    $this->load->model('JadwalModel');
    return $this->JadwalModel->get(false, ['right(id_jadwal_lapangan, 2) ASC']);
  }

  private function generateJam()
  {

  }
}
