<?php
namespace App\Validations;

class AdminValidation extends Validation
{
  protected $config = [
    "nama_admin" => ['nama_admin', 'Nama', 'trim|required|min_length[3]|max_length[40]|alpha_numeric_spaces'],
    "username" => ['username', 'Username', 'trim|required'],
    "password" => ['password', 'Password', 'trim|min_length[6]|max_length[8]|required'],
    "re-password" => ['re-password', 'Konfirmasi Password', 'trim|required|matches[password]'],
    "tipe-akun" => ['tipe-akun', 'Tipe Akun', 'required'],
  ];

  public function except()
  {
    $id = $this->instance->uri->segment(2);
    $value = $this->instance->input->post('username');
    $dt = $this->instance->db->select('username')->where('id_admin', $id)->where('username', $value)->limit(1)->get('admin');

    if(!($dt->num_rows())) {
      $this->config['username'][2] .= "|is_unique[admin.username]";
    }

    $password = $this->instance->input->post('password');
    $repassword = $this->instance->input->post('re-password');

    if(($value && $id) || (empty($value) && !empty($id))) {
      if(empty($password) && empty($repassword)) {
        unset($this->config['password']);
        unset($this->config['re-password']);
      }

      unset($this->config['tipe-akun']);
    }
  }

  public function getErrors()
  {
    $errors = [
        'nama_admin' => form_error('nama_admin'),
        'username' => form_error('username'),
        'password' => form_error('password'),
        're-password' => form_error('re-password'),
        'tipe-akun' => form_error('tipe-akun'),
      ];

      foreach($errors as $key => $value) {
        if(!($errors[$key])) {
          unset($errors[$key]);
        }
      }

      return $errors;
  }

  public function getOldValue()
  {
    $value = [
      'nama_admin' => set_value('nama_admin'),
      'username' => set_value('username'),
      'tipe-akun' => set_value('tipe-akun')
    ];

      return $value;
  }
}
