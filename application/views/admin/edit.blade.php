@extends("layouts.global")

@section("title", "Edit Admin")

@section("content")
    <div class="row">
        <div class="col-md-8">
            @component('components.flash.notify')
            @endcomponent

            <form action="{{ base_url("admin/{$data->id_admin}/update") }}" method="POST" class="shadow-sm p-3 bg-white">
                <div class="form-group">
                    @component('components.form.input', [
                        'type' => 'text',
                        'id' => 'nama-adm',
                        'placeholder' => 'Nama',
                        'name' => 'nama_admin',
                        'label' => 'Nama Admin',
                        'data' => $data
                    ])
                        
                    @endcomponent
                </div>

                <div class="form-group">
                    @component('components.form.input', [
                        'type' => 'text',
                        'id' => 'username-adm',
                        'placeholder' => 'Username',
                        'name' => 'username',
                        'label' => 'Username',
                        'data' => $data
                    ])
                        
                    @endcomponent
                </div>

                <div class="form-group">
                    @component('components.form.input', [
                        'type' => 'password',
                        'id' => 'password-adm',
                        'placeholder' => 'Password Admin',
                        'name' => 'password',
                        'label' => 'Password',
                        'data' => $data
                    ])
                        
                    @endcomponent
                </div>

                <div class="form-group">
                    @component('components.form.input', [
                        'type' => 'password',
                        'id' => 're-password-adm',
                        'placeholder' => 'Re-Password Admin',
                        'name' => 're-password',
                        'label' => 'Re-Password',
                        'data' => $data
                    ])
                        
                    @endcomponent
                </div>
                
                <div class="form-group">
                    {!! getCSRFToken() !!}
                    @component('components.buttons.submit', ['text' => 'Rubah', 'type' => 'primary'])@endcomponent
                    @component('components.buttons.cancel', ['text' => 'Batal', 'type' => 'secondary', 'link' => 'admin'])@endcomponent
                </div>
            </form>
        </div>
</div>
@endsection