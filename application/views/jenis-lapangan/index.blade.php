@extends('layouts.global')

@section('title', "List Jenis Lapangan")

@section('pageTitle', 'List Jenis Lapangan')
@section('pageTitleIcon', 'fa-tags')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <hr class="my-3">
            <div class="row">
                <div class="col-md-12">
                    @component('components.flash.notify')
                    @endcomponent
                </div>
            </div>
            @if(checkAccess())
	            <div class="row mb-3">
	                <div class="col-md-12 text-right">
	                    @component('components.buttons.add', ['link' => 'jenis-lapangan/add', 'text' => 'Tambah Jenis Lapangan'])
	                    @endcomponent
	                </div>
	            </div>
	        @endif

            <table class="table table-bordered table-stripped">
                <thead>
                    <tr>
                        <th width="10"><strong>No</strong></th>
                        <th><strong>Nama Jenis Lapangan</strong></th>
                        <th><strong>Harga</strong></th>
                        @if(checkAccess())
                        	<th><strong>Aksi</strong></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @php $counter = 1; @endphp
                    @foreach($data as $jlp)
                        <tr>
                            <td class="text-center">{{ $counter }}</td>
                            <td class="text-capitalize">{{$jlp->nama_jenis_lapangan}}</td>
                            <td>Rp. {{ number_format($jlp->harga, 0, ",", ".") }}</td>
                            @if(checkAccess())
	                            <td>
	                                @component('components.buttons.action', [
	                                    'linkUpdate' => "jenis-lapangan/{$jlp->id_jenis_lapangan}/update",
	                                    'linkDelete' => "jenis-lapangan/{$jlp->id_jenis_lapangan}/destroy"
	                                ])

	                                @endcomponent
	                            </td>
	                        @endif
                        </tr>
                        @php $counter++ @endphp
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">

                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
